package sample;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;


/**
 * Класс, содержащий операцию деления.
 */
public class Calculator {
    /**
     * Метод выполняющий деление. Содержит валидацию введенных значений.
     *
     * @param dividend делимое.
     * @param divider  делитель.
     * @return частное.
     */
    public static BigDecimal divide(String dividend, String divider) {
        if (StringUtils.isBlank(dividend)) {
            throw new IllegalArgumentException("Dividend is empty");

        }
        if (StringUtils.isBlank(divider)) {
            throw new IllegalArgumentException("Divider is empty");
        }

        Validate.matchesPattern(dividend, "[0-9]+[\\.]?[0-9]*");
        Validate.matchesPattern(divider, "[0-9]+[\\.]?[0-9]*");

        BigDecimal x = new BigDecimal(dividend);
        BigDecimal y = new BigDecimal(divider);

        return x.divide(y);
    }
}
