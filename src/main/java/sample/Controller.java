package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.math.BigDecimal;


/**
 * Класс, обрабатывающий взаимодействие с формой.
 */
public class Controller {
    @FXML
    private TextField dividend;
    @FXML
    private TextField divider;
    @FXML
    private TextField qoutient;
    @FXML
    private Label error;


    /**
     * Обработчик нажатия кнопки "=".
     *
     * @param actionEvent событие.
     */
    public void calculate(ActionEvent actionEvent) {
        try {
            BigDecimal divide = Calculator.divide(dividend.getText(), divider.getText());
            qoutient.setText(divide.toPlainString());
        } catch (IllegalArgumentException | ArithmeticException e) {
            error.setText(e.getMessage());
        }
    }
}
