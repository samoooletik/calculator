package sample

import spock.lang.Specification
import spock.lang.Unroll

class CalculatorTest extends Specification {

    @Unroll
    def "Проверка выполнения деления #dividend / #diviser = #result"(String dividend, String diviser, BigDecimal result) {
        expect: "Проверка правильности вычисления"
        result == Calculator.divide(dividend, diviser)

        where:
        dividend | diviser | result
        '12'     | '6'     | new BigDecimal('2')
        '12.2'   | '2'     | new BigDecimal('6.1')
        '12'     | '1.5'   | new BigDecimal('8')
        '2.2'    | '2.2'   | new BigDecimal('1')
        '0'      | '2.2'   | new BigDecimal('0')
        '5'      | '10'    | new BigDecimal('0.5')
    }

    @Unroll
    def "Проверка валидации неправильных значений #dividend / #diviser"(String dividend, String diviser) {
        when: "Попытка выполнить метод"
        Calculator.divide(dividend, diviser)

        then: "Получение ошибки валидации"
        thrown(IllegalArgumentException)

        where:
        dividend | diviser
        'aasd'   | '2'
        '33'     | 'as'
        'ww'     | 'ff'
        '1,5'    | '2'
        '3'      | '1,5'
        '3,5'    | '3,5'
    }

    def "Проверка деления на ноль"() {
        when: 'Попытка деления на ноль'
        Calculator.divide('1221', '0')

        then: 'Ошибка валидации'
        thrown(ArithmeticException)
    }
}
